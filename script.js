// change active button on click
const btns = document.querySelectorAll(".portfolio-tabs button");
btns.forEach((btn) => {
    btn.addEventListener("click", (e) => {
        btns.forEach((btn) => btn.classList.remove("active"));
        e.target.classList.add("active");
    });
    }
);